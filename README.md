# Update-FTPExternalIP.ps1

A small PowerShell script to automatically update the ftpServer/firewallSupport/externalIp4Address value in IIS when an IP change is detected, for FTP setups on hosts with dynamic IP addresses.

## Usage

For a very basic and lazy setup, follow the directions below. If you have a dedicated device for automation tasks this script is easily modified to run remotely. I didn't write this in because among us who don't have a static IP that won't be common.

1. Download the script to the Windows server, place it in your desired location and set permissions (only server administrators should have modify)
2. Create a scheduled task to run the script at your desired frequency.
3. Under the *Actions* tab set `powershell.exe` as the program/script
4. Under arguments set the following, choosing the correct location for the script, and your desired location for the log file (note that the path for the log file must exist, but the file itself will be created) `-command "C:\path\to\script\Update-FTPExternalIP.ps1 | format-list >> C:\path\to\logs\directory\Update-FTPExternalIP.log"`